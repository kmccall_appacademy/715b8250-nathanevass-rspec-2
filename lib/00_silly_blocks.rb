def reverser(&string)
  string.call.split.map(&:reverse).join(" ")
end 

def adder(n = 1, &prc_val)
  prc_val.call + n 
end 

def repeater(times = 1, &proc)
  times.times {proc.call}
end 